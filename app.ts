/**
 * * Exercises
 */

// * Common Objects
class Animal {
  constructor(public age: number) {}
  public eat(food: string): boolean {
    return Boolean(food);
  }
}

// Helper: Use this to log to console!!!
const log = console.log;


// ? Exercise 1
// Create a shark that is of type Fish which has 'fins' (number)
// Note: shark is also an Animal
interface Fish { fins: number}


// ? Exercise 2
// Create a function that checks if an object is of Animal type and
// test it on 'lion' below
const lion = {
  age: 5,
  eat(whatever) {
    return true;
  },
};


// ? Exercise 3
// Uncomment, Fix and Test function below
// function isAnimal(animal: unknown): boolean {
//   return 'age' in animal;
// }


// ? Exercise 4
// Fill in zoo with 1 `animal`, 1 `fish` and 1 `{ age: 4 }`
// Observe ElemType while creating the zoo!
const zoo = [];
type ElemType = typeof zoo[number];


// ? Exercise 5
// Use type guard below to filter fish out of zoo
// Note: assign the result to 'lotsOfFish' and observe!
function isFish(pet: Fish | Animal): pet is Fish {
  return (pet as Fish).fins !== undefined;
}


// ? Exercice 6
// Uncomment code, Find the error and Fix it!
// function hasFins(x: any = {fins: 3}): boolean {
//   return !!x['fins'];
// }
// log('hasFins', hasFins());


// ? Exercise 7
// Add types on the function below using Animal class
// Call make and observe the result
function make(X, a) {
  return new X(a);
}
